package com.AdminService.repository;

import org.springframework.stereotype.Repository;

import com.AdminService.entity.Admin;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AdminServicerepo extends JpaRepository<Admin,String> {

}
