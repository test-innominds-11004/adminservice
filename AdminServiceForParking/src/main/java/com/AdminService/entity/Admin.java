package com.AdminService.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="AdminService")
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="count")
	private int Count;
	@Column(name="vehicletype")
	private String VehicleType;
	public String getVehicleType() {
		return VehicleType;
	}
	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}
	public int getCount() {
		return Count;
	}
	public void setCount(int count) {
		Count = count;
	}
}