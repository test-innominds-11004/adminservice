package com.AdminService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminServiceForParkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminServiceForParkingApplication.class, args);
	}

}
